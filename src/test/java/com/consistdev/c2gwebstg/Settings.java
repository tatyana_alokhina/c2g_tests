package com.consistdev.c2gwebstg;

import javafx.util.Pair;

public class Settings {
    public static final boolean IS_WORK_BUILD = true;

    public static final String EMAIL_PRIVATE_MEMBER = "010255222";
    public static final String PASSWORD = "1qaz2wsx";

    public static final String BASE_URL = "http://c2gwebstg.consistdev.com:8080";
    public static final String LOGIN_URL = Settings.BASE_URL + "/member/login";
    public static final String RESERVATION_URL = Settings.BASE_URL + "/reservation";

    public static String WEB_DRIVER_PATH;
    public static String WEB_DRIVER;

    public Settings() {
        Pair<String, String> webDriverConfig = getWebDriverConfig("chrome");
        WEB_DRIVER = webDriverConfig.getKey();
        String WEB_DRIVER_WORK_PATH = "D:\\Test\\_projects\\" + webDriverConfig.getValue() + ".exe";
        String WEB_DRIVER_HOME_PATH = "C:\\Users\\Katahezis\\IdeaProjects\\" + webDriverConfig.getValue() + ".exe";
        WEB_DRIVER_PATH = IS_WORK_BUILD ? WEB_DRIVER_WORK_PATH : WEB_DRIVER_HOME_PATH;
    }

    private Pair<String, String> getWebDriverConfig(String browser){
        if (browser.equals("chrome")) {
            return new Pair<String, String>("webdriver.chrome.driver", "chromedriver");
        } else if (browser.equals("opera")){

        }else if (browser.equals("explorer")){

        }else if (browser.equals("firefox")){

        }
        return null;
    }
}



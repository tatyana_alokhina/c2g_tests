package com.consistdev.c2gwebstg.login;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static com.consistdev.c2gwebstg.Settings.*;
import static junit.framework.Assert.assertEquals;

public class Car2goLogin {
    private WebDriver driver;
    private ConfigLogin configLogin = new ConfigLogin();

    @Before
    public void createDriver() {
        System.setProperty(WEB_DRIVER, WEB_DRIVER_PATH);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void testLogin() {
        driver.get(LOGIN_URL);
        driver.findElement(By.id("loginform-identity_id")).sendKeys(ConfigLogin.EMAIL_PRIVATE_MEMBER);
        driver.findElement(By.id("loginform-password")).sendKeys(ConfigLogin.PASSWORD);
        driver.findElement(By.className("big_btn__btn")).click();
        driver.findElement(By.id("member-private-4019001")).click();

        assertEquals(RESERVATION_URL, driver.getCurrentUrl());
    }

    @Test
    public void testLoginWithEmptyLogin() {
        driver.get(LOGIN_URL);
        driver.findElement(By.className("eng")).click();
        driver.findElement(By.id("loginform-password")).sendKeys(ConfigLogin.PASSWORD);
        driver.findElement(By.className("big_btn__btn")).click();
        WebElement errorText = driver.findElement(By.className("uk-margin-top-remove"));

        assertEquals("One of the details incorrect", errorText.getText());
    }

    @Test
    public void testLoginWithEmptyPass() {
        driver.get(LOGIN_URL);
        driver.findElement(By.className("eng")).click();
        driver.findElement(By.id("loginform-identity_id")).sendKeys(ConfigLogin.EMAIL_PRIVATE_MEMBER);
        driver.findElement(By.className("big_btn__btn")).click();
        WebElement errorText = driver.findElement(By.className("uk-margin-top-remove"));

        assertEquals("One of the details incorrect", errorText.getText());
    }

    @After
    public void closeDriver() {
        driver.close();
        driver.quit();
    }
}
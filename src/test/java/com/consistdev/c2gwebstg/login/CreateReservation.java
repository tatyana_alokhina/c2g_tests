package com.consistdev.c2gwebstg.login;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import static com.consistdev.c2gwebstg.Settings.LOGIN_URL;
import static com.consistdev.c2gwebstg.Settings.WEB_DRIVER;
import static com.consistdev.c2gwebstg.Settings.WEB_DRIVER_PATH;


public class CreateReservation {
    private WebDriver driver;

    @Before
    public void createDriver() {
        System.setProperty(WEB_DRIVER, WEB_DRIVER_PATH);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void createReservation() throws Exception {
        driver.get(LOGIN_URL);
        driver.findElement(By.className("eng")).click();
        driver.findElement(By.id("loginform-identity_id")).sendKeys(ConfigLogin.EMAIL_PRIVATE_MEMBER);
        driver.findElement(By.id("loginform-password")).sendKeys(ConfigLogin.PASSWORD);
        driver.findElement(By.className("big_btn__btn")).click();
        driver.findElement(By.id("member-private-4019001")).click();
        driver.findElement(By.id("start-date")).click();
        driver.findElement(By.xpath(".//*[@id='multiple']//div[35]")).click();
        driver.findElement(By.xpath(".//*[@id='pick-start-date']//div[3]/button")).click();
        driver.findElement(By.id("end-date")).click();
        driver.findElement(By.xpath(".//*[@id='pick-end-date']//div")).click();
        driver.findElement(By.xpath(".//*[@id='pick-end-date']/div/div[3]/button")).click();
        driver.findElement(By.id("go_search")).click();
        driver.findElement(By.cssSelector(".uk-button.orange_btn.uk-text-center>a")).click();
        Thread.sleep(5000);
        /*driver.switchTo().alert().;
        driver.findElement(By.cssSelector("css=#car-info > div.modal-btn-container > button.uk-button.orange_btn"));*/
        //

        //driver.switchTo().alert();
        //WebDriverWait wait = new WebDriverWait(driver, 30);
        //wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#car-info")));

        //driver.findElement(By.xpath("//div[15]/div[2]/button")).click();
    }


    //driver.findElement(By.xpath(".//*[@id='car-info']/div[2]/button")).click();

    //WebDriverWait wait = new WebDriverWait(driver, 5);
    //Alert alert = wait.until(ExpectedConditions.alertIsPresent());
    //driver.switchTo().alert();
    //Thread.sleep(5000);
    //driver.findElement(By.xpath(".//*[@id='car-info']/div[2]/button")).click();

    @After
    public void closeBrowser() {
        driver.close();
        driver.quit();
    }
}

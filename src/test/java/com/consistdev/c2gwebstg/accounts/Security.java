package com.consistdev.c2gwebstg.accounts;

import com.consistdev.c2gwebstg.Settings;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static com.consistdev.c2gwebstg.Settings.LOGIN_URL;
import static com.consistdev.c2gwebstg.Settings.WEB_DRIVER;
import static com.consistdev.c2gwebstg.Settings.WEB_DRIVER_PATH;
import static com.consistdev.c2gwebstg.accounts.selectors.SelectorsProfile.*;
import static com.consistdev.c2gwebstg.accounts.selectors.SelectorsProfile.CHOOSE_MEMBER_ID;
import static com.consistdev.c2gwebstg.accounts.selectors.SelectorsSecurity.*;
import static junit.framework.Assert.assertEquals;

public class Security {
    private static WebDriver driver;
    private static ConfigAccounts configAccounts = new ConfigAccounts();

    @Before
    public void createDriverAndLogin() {
        System.setProperty(WEB_DRIVER, WEB_DRIVER_PATH);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(LOGIN_URL);
        driver.findElement(By.className(ENGLISH_CN)).click();
        driver.findElement(By.id(LOGIN_FIELD_ID)).sendKeys(Settings.EMAIL_PRIVATE_MEMBER);

    }

    //Checking changing password positive test
    //For testing this you should replace values of the variables "OLD_PASSWORD" and "NEW_PASSWORD" in class "SelectorsSecurity"
    @Test
    public void changingPasswordPositiveTest() throws InterruptedException {
        driver.findElement(By.id(PASSWORD_FIELD_ID)).sendKeys(OLD_PASSWORD); // this variable you can change in class "SelectorsSecurity"
        driver.findElement(By.className(BUTTON_LOGIN_CN)).click();
        if (OLD_PASSWORD.equals("1qaz2wsx")) { //this value was taking from "OLD_PASSWORD" in class "SelectorsSecurity"
            driver.findElement(By.id(CHOOSE_MEMBER_ID)).click();
        }
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath(SECURITY_LINK_XPATH)).click();
        driver.findElement(By.id(SECURITY_OLD_PASSWORD_FIELD_ID)).sendKeys(OLD_PASSWORD); // this variable you can change in class "SelectorsSecurity"
        driver.findElement(By.id(SECURITY_NEW_PASSWORD_FIELD_ID)).sendKeys(NEW_PASSWORD); // this variable you can change in class "SelectorsSecurity"
        driver.findElement(By.id(SECURITY_CONFIRM_NEW_PASSWORD_FIELD_ID)).sendKeys(NEW_PASSWORD); // this variable you can change in class "SelectorsSecurity"
        driver.findElement(By.xpath(SECURITY_SAVE_CHANGES_BUTTON_XPATH)).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath(LOG_OUT_XPATH)).click();
        driver.findElement(By.id(LOGIN_FIELD_ID)).sendKeys(Settings.EMAIL_PRIVATE_MEMBER);
        driver.findElement(By.id(PASSWORD_FIELD_ID)).sendKeys(NEW_PASSWORD); // this variable you can change in class "SelectorsSecurity"
        Thread.sleep(5000);
        driver.findElement(By.className(BUTTON_LOGIN_CN)).click();
        Thread.sleep(5000);
        if (NEW_PASSWORD.equals("1qaz2wsx")) { //this value was taking from "NEW_PASSWORD" in class "SelectorsSecurity"
            driver.findElement(By.id(CHOOSE_MEMBER_ID)).click();
        }
        Thread.sleep(5000);
        System.out.println(NEW_PASSWORD);
        assertEquals("http://c2gwebstg.consistdev.com:8080/reservation", driver.getCurrentUrl());

    }

    //Passwords do not match. Checking appearing message in English.
    @Test
    public void conformationPasswordDoNotMatchNewPasswordEN() throws InterruptedException {
        driver.findElement(By.id(PASSWORD_FIELD_ID)).sendKeys(NEW_PASSWORD); // this variable you can change in class "SelectorsSecurity"
        driver.findElement(By.className(BUTTON_LOGIN_CN)).click();
        Thread.sleep(5000);
        if (NEW_PASSWORD.equals("1qaz2wsx")) { //this value was taking from "OLD_PASSWORD" in class "SelectorsSecurity"
            driver.findElement(By.id(CHOOSE_MEMBER_ID)).click();
        }
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath(SECURITY_LINK_XPATH)).click();
        driver.findElement(By.id(SECURITY_OLD_PASSWORD_FIELD_ID)).sendKeys(NEW_PASSWORD); // this variable you can change in class "SelectorsSecurity"
        driver.findElement(By.id(SECURITY_NEW_PASSWORD_FIELD_ID)).sendKeys("1234567a"); // this variable you can change in class "SelectorsSecurity"
        driver.findElement(By.id(SECURITY_CONFIRM_NEW_PASSWORD_FIELD_ID)).sendKeys("1234567b"); // this variable you can change in class "SelectorsSecurity"
        driver.findElement(By.xpath(SECURITY_SAVE_CHANGES_BUTTON_XPATH)).click();
        Thread.sleep(5000);
        WebElement messagePasswordMustMatch = driver.findElement(By.className("orange-text"));
        assertEquals("Passwords must match", messagePasswordMustMatch.getText());
    }

    //Passwords do not match. Checking appearing message in Hebrew.
    @Test
    public void conformationPasswordDoNotMatchNewPasswordHebrew() throws InterruptedException {
        driver.findElement(By.id(PASSWORD_FIELD_ID)).sendKeys(NEW_PASSWORD); // this variable you can change in class "SelectorsSecurity"
        driver.findElement(By.className(BUTTON_LOGIN_CN)).click();
        Thread.sleep(5000);
        if (NEW_PASSWORD.equals("1qaz2wsx")) { //this value was taking from "OLD_PASSWORD" in class "SelectorsSecurity"
            driver.findElement(By.id(CHOOSE_MEMBER_ID)).click();
        }
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath(SECURITY_LINK_XPATH)).click();
        driver.findElement(By.className(SELECT_HEBREW_CN)).click();
        driver.findElement(By.id(SECURITY_OLD_PASSWORD_FIELD_ID)).sendKeys(NEW_PASSWORD); // this variable you can change in class "SelectorsSecurity"
        driver.findElement(By.id(SECURITY_NEW_PASSWORD_FIELD_ID)).sendKeys("1234567a"); // this variable you can change in class "SelectorsSecurity"
        driver.findElement(By.id(SECURITY_CONFIRM_NEW_PASSWORD_FIELD_ID)).sendKeys("1234567b"); // this variable you can change in class "SelectorsSecurity"
        driver.findElement(By.xpath(SECURITY_SAVE_CHANGES_BUTTON_XPATH)).click();
        Thread.sleep(5000);
        WebElement messagePasswordMustMatch = driver.findElement(By.className("orange-text"));
        assertEquals("הססמאות חייבות להיות זהות", messagePasswordMustMatch.getText());
    }


    //Live all fields empty. Checking appearing message in English.
    @Test
    public void changePasswordAndLiveEmptyFieldsEN() throws InterruptedException {
        driver.findElement(By.id(PASSWORD_FIELD_ID)).sendKeys(NEW_PASSWORD); // this variable you can change in class "SelectorsSecurity"
        driver.findElement(By.className(BUTTON_LOGIN_CN)).click();
        Thread.sleep(5000);
        if (NEW_PASSWORD.equals("1qaz2wsx")) { //this value was taking from "OLD_PASSWORD" in class "SelectorsSecurity"
            driver.findElement(By.id(CHOOSE_MEMBER_ID)).click();
        }
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath(SECURITY_LINK_XPATH)).click();
        driver.findElement(By.xpath(SECURITY_SAVE_CHANGES_BUTTON_XPATH)).click();
        Thread.sleep(5000);
        WebElement messageYouMustFillInTheRequiredField = driver.findElement(By.className("orange-text"));
        assertEquals("You must fill in the required field", messageYouMustFillInTheRequiredField.getText());
    }

    //Live all fields empty. Checking appearing message in Hebrew.
    @Test
    public void changePasswordAndLiveEmptyFieldsHebrew() throws InterruptedException {
        driver.findElement(By.id(PASSWORD_FIELD_ID)).sendKeys(NEW_PASSWORD); // this variable you can change in class "SelectorsSecurity"
        driver.findElement(By.className(BUTTON_LOGIN_CN)).click();
        Thread.sleep(5000);
        if (NEW_PASSWORD.equals("1qaz2wsx")) { //this value was taking from "OLD_PASSWORD" in class "SelectorsSecurity"
            driver.findElement(By.id(CHOOSE_MEMBER_ID)).click();
        }
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath(SECURITY_LINK_XPATH)).click();
        driver.findElement(By.className(SELECT_HEBREW_CN)).click();
        driver.findElement(By.xpath(SECURITY_SAVE_CHANGES_BUTTON_XPATH)).click();
        Thread.sleep(5000);
        WebElement messageYouMustFillInTheRequiredField = driver.findElement(By.className("orange-text"));
        assertEquals("עליך למלא את שדה החובה", messageYouMustFillInTheRequiredField.getText());
    }

    //Enter wrong old password (English)
    @Test
    public void wrongOldPasswordEN() throws InterruptedException {
        driver.findElement(By.id(PASSWORD_FIELD_ID)).sendKeys(NEW_PASSWORD); // this variable you can change in class "SelectorsSecurity"
        driver.findElement(By.className(BUTTON_LOGIN_CN)).click();
        Thread.sleep(5000);
        if (NEW_PASSWORD.equals("1qaz2wsx")) { //this value was taking from "OLD_PASSWORD" in class "SelectorsSecurity"
            driver.findElement(By.id(CHOOSE_MEMBER_ID)).click();
        }
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath(SECURITY_LINK_XPATH)).click();
        driver.findElement(By.id(SECURITY_OLD_PASSWORD_FIELD_ID)).sendKeys("0000000h");
        driver.findElement(By.id(SECURITY_NEW_PASSWORD_FIELD_ID)).sendKeys("1234567a");
        driver.findElement(By.id(SECURITY_CONFIRM_NEW_PASSWORD_FIELD_ID)).sendKeys("1234567a");
        driver.findElement(By.xpath(SECURITY_SAVE_CHANGES_BUTTON_XPATH)).click();
        Thread.sleep(5000);
        WebElement messagePasswordMustMatch = driver.findElement(By.className("orange-text"));
        assertEquals("Old password is incorrect", messagePasswordMustMatch.getText());
    }

    //Enter wrong old password (Hebrew)
    @Test
    public void wrongOldPasswordHebrew() throws InterruptedException {
        driver.findElement(By.id(PASSWORD_FIELD_ID)).sendKeys(NEW_PASSWORD); // this variable you can change in class "SelectorsSecurity"
        driver.findElement(By.className(BUTTON_LOGIN_CN)).click();
        Thread.sleep(5000);
        if (NEW_PASSWORD.equals("1qaz2wsx")) { //this value was taking from "OLD_PASSWORD" in class "SelectorsSecurity"
            driver.findElement(By.id(CHOOSE_MEMBER_ID)).click();
        }
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath(SECURITY_LINK_XPATH)).click();
        driver.findElement(By.className(SELECT_HEBREW_CN)).click();
        driver.findElement(By.id(SECURITY_OLD_PASSWORD_FIELD_ID)).sendKeys("0000000h");
        driver.findElement(By.id(SECURITY_NEW_PASSWORD_FIELD_ID)).sendKeys("1234567a");
        driver.findElement(By.id(SECURITY_CONFIRM_NEW_PASSWORD_FIELD_ID)).sendKeys("1234567a");
        driver.findElement(By.xpath(SECURITY_SAVE_CHANGES_BUTTON_XPATH)).click();
        Thread.sleep(5000);
        WebElement messagePasswordMustMatch = driver.findElement(By.className("orange-text"));
        assertEquals("הסיסמא הישנה אינה נכונה", messagePasswordMustMatch.getText());
    }

    /*// Checking opening Security page
    @Test
    public void openProfilePage() throws InterruptedException {
        driver.findElement(By.id(PASSWORD_FIELD_ID)).sendKeys(Settings.PASSWORD);
        driver.findElement(By.className(BUTTON_LOGIN_CN)).click();
        driver.findElement(By.id(CHOOSE_MEMBER_ID)).click();
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        driver.findElement(By.xpath(SECURITY_LINK_XPATH)).click();
        assertEquals("http://c2gwebstg.consistdev.com:8080/account/security/index", driver.getCurrentUrl());
    }*/


    /*//Checking that field "Username" isn't empty
    @Test
    public void usernameIsPresent() throws InterruptedException {
        driver.findElement(By.id(PASSWORD_FIELD_ID)).sendKeys(Settings.PASSWORD);
        driver.findElement(By.className(BUTTON_LOGIN_CN)).click();
        driver.findElement(By.id(CHOOSE_MEMBER_ID)).click();
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        driver.findElement(By.xpath(SECURITY_LINK_XPATH)).click();
        Thread.sleep(6000);
        String username = driver.findElement(By.cssSelector(USERNAME_FIELD_IN_SECURITY_PAGE_CSS)).getText();
        assert !username.equals("");
    }*/


    @After
    public void closeBrowser() {
        driver.close();
        driver.quit();
    }
}

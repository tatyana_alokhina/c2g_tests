package com.consistdev.c2gwebstg.accounts;

import com.consistdev.c2gwebstg.Settings;

public class ConfigAccounts extends Settings{
    // Profile
    //Path to photos
    public final String JPG_PHOTO;
    public final String PNG_PHOTO;
    public final String GIF_PHOTO;
    public final String BIG_PNG_PHOTO;

    //Change address
    public static String NEW_ADDRESS = "Paris, France";

    public ConfigAccounts() {
        if (IS_WORK_BUILD) {
            JPG_PHOTO = "C:\\Users\\Admin\\Desktop\\test photo\\a.jpg";
            PNG_PHOTO = "C:\\Users\\Admin\\Desktop\\test photo\\a.png";
            GIF_PHOTO = "C:\\Users\\Admin\\Desktop\\test photo\\a.gif";
            BIG_PNG_PHOTO = "C:\\Users\\Admin\\Desktop\\test photo\\big.png";
        } else {
            JPG_PHOTO = "";
            PNG_PHOTO = "";
            GIF_PHOTO = "";
            BIG_PNG_PHOTO = "";
        }
    }

}

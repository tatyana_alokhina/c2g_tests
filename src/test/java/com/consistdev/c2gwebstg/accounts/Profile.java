package com.consistdev.c2gwebstg.accounts;

import com.consistdev.c2gwebstg.Settings;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.List;

import static com.consistdev.c2gwebstg.Settings.LOGIN_URL;
import static com.consistdev.c2gwebstg.Settings.WEB_DRIVER;
import static com.consistdev.c2gwebstg.Settings.WEB_DRIVER_PATH;
import static com.consistdev.c2gwebstg.accounts.selectors.SelectorsProfile.*;
import static junit.framework.Assert.assertEquals;

public class Profile {
    private static WebDriver driver;
    private static ConfigAccounts configAccounts = new ConfigAccounts();

    @Before
    public void createDriverAndLogin() {
        System.setProperty(WEB_DRIVER, WEB_DRIVER_PATH);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(LOGIN_URL);
        driver.findElement(By.className(ENGLISH_CN)).click();
        driver.findElement(By.id(LOGIN_FIELD_ID)).sendKeys(Settings.EMAIL_PRIVATE_MEMBER);
        driver.findElement(By.id(PASSWORD_FIELD_ID)).sendKeys(Settings.PASSWORD);
        driver.findElement(By.className(BUTTON_LOGIN_CN)).click();
        driver.findElement(By.id(CHOOSE_MEMBER_ID)).click();
    }

    // Checking opening Profile page
    @Test
    public void openProfilePage() {
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        driver.findElement(By.cssSelector(PROFILE_LINK_CSS)).click();
    }

    //Checking that field "Name" isn't empty
    @Test
    public void nameIsPresent() {
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        driver.findElement(By.cssSelector(PROFILE_LINK_CSS)).click();
        driver.findElement(By.cssSelector(EDITING_BUTTON_CSS)).click();
        String nameField = driver.findElement(By.cssSelector(NAME_FIELD_CSS)).getText();
        assert !nameField.equals("");
    }

    //Checking that field "Account number" isn't empty
    @Test
    public void accountNumberIsPresent() {
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        driver.findElement(By.cssSelector(PROFILE_LINK_CSS)).click();
        driver.findElement(By.cssSelector(EDITING_BUTTON_CSS)).click();
        String accountNumber = driver.findElement(By.xpath(ACCOUNT_NUMBER_FIELD_XPATH)).getText();
        assert !accountNumber.equals("");
    }

    //Checking that field "E-mail" isn't empty
    @Test
    public void emailIsPresent() {
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        driver.findElement(By.cssSelector(PROFILE_LINK_CSS)).click();
        driver.findElement(By.cssSelector(EDITING_BUTTON_CSS)).click();
        String email = driver.findElement(By.xpath(EMAIL_FIELD_XPATH)).getText();
        assert !email.equals("");
    }

    //Checking that field "Date of birth" isn't empty
    @Test
    public void dateOfBirthIsPresent() {
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        driver.findElement(By.cssSelector(PROFILE_LINK_CSS)).click();
        driver.findElement(By.cssSelector(EDITING_BUTTON_CSS)).click();
        String dateOfBirth = driver.findElement(By.xpath(DATE_OF_BIRTH_FIELD_XPATH)).getText();
        assert !dateOfBirth.equals("");
    }

    //Checking saving new address after fixing API bug
    //
    //


    // Checking appearing API errors after uploading new profile photo with .jpg format
    @Test
    public void changeProfilePhotoWithJpgFormatApiErrorTest() throws InterruptedException {
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        driver.findElement(By.cssSelector(PROFILE_LINK_CSS)).click();
        driver.findElement(By.cssSelector(EDITING_BUTTON_CSS)).click();
        driver.findElement(By.id(BROWSE_PROFILE_PHOTO_ID)).sendKeys(configAccounts.JPG_PHOTO);
        driver.findElement(By.cssSelector(SAVING_BUTTON_CSS)).click();
        WebElement popupWithApiError = driver.findElement(By.cssSelector(POPUP_WITH_API_ERROR_CSS));
        if (popupWithApiError.getAttribute("innerHTML") != "") {
            System.out.println("API error occurred");
        }
        assertEquals("", popupWithApiError.getText());
    }

    // Checking appearing API errors after uploading new profile photo with .png format
    @Test
    public void changeProfilePhotoWithPngFormatApiErrorTest() throws InterruptedException {
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        driver.findElement(By.cssSelector(PROFILE_LINK_CSS)).click();
        driver.findElement(By.cssSelector(EDITING_BUTTON_CSS)).click();
        driver.findElement(By.id(BROWSE_PROFILE_PHOTO_ID)).sendKeys(configAccounts.PNG_PHOTO);
        driver.findElement(By.cssSelector(SAVING_BUTTON_CSS)).click();
        WebElement popupWithApiError = driver.findElement(By.cssSelector(POPUP_WITH_API_ERROR_CSS));
        if (popupWithApiError.getAttribute("innerHTML") != "") {
            System.out.println("API error occurred");
        }
        assertEquals("", popupWithApiError.getText());
    }

    // Checking appearing API errors after uploading new profile photo with .gif format
    @Test
    public void changeProfilePhotoWithGifFormatApiErrorTest() throws InterruptedException {
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        driver.findElement(By.cssSelector(PROFILE_LINK_CSS)).click();
        driver.findElement(By.cssSelector(EDITING_BUTTON_CSS)).click();
        driver.findElement(By.id(BROWSE_PROFILE_PHOTO_ID)).sendKeys(configAccounts.GIF_PHOTO);
        driver.findElement(By.cssSelector(SAVING_BUTTON_CSS)).click();
        WebElement popupWithApiError = driver.findElement(By.cssSelector(POPUP_WITH_API_ERROR_CSS));
        if (popupWithApiError.getAttribute("innerHTML") != "") {
            System.out.println("API error occurred");
        }
        assertEquals("", popupWithApiError.getText());
    }

    // Checking appearing error message after uploading new profile photo with size more 2MB
    @Test
    public void checkingOfErrorMessageIfUploadToBigProfilePhoto() {
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        driver.findElement(By.cssSelector(PROFILE_LINK_CSS)).click();
        driver.findElement(By.cssSelector(EDITING_BUTTON_CSS)).click();
        driver.findElement(By.id(BROWSE_PROFILE_PHOTO_ID)).sendKeys(configAccounts.BIG_PNG_PHOTO);
        driver.findElement(By.cssSelector(SAVING_BUTTON_CSS)).click();
        WebElement errorMessageToBigProfilePhoto = driver.findElement(By.cssSelector(ERROR_MESSAGE_TO_BIG_PROFILE_PHOTO_CSS));
        assertEquals("The file is too big. Its size cannot exceed 2MB", errorMessageToBigProfilePhoto.getText());
    }

    // Checking appearing error message if delete address
    @Test
    public void checkingOfErrorMessageIfDeleteAddress() throws InterruptedException {
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        driver.findElement(By.cssSelector(PROFILE_LINK_CSS)).click();
        driver.findElement(By.cssSelector(EDITING_BUTTON_CSS)).click();
        WebElement address = driver.findElement(By.id(ADDRESS_FIELD_ID));
        address.clear();
        driver.findElement(By.cssSelector(NAME_FIELD_CSS)).click();
        Thread.sleep(5000);
        WebElement errorMessageTheAddressDoesNotExist = driver.findElement(By.cssSelector(ERROR_MESSAGE_THE_ADDRESS_DOES_NOT_EXIST_CSS));
        assertEquals("The address does not exist", errorMessageTheAddressDoesNotExist.getText());
    }

    // Checking appearing error message if try to save empty Address
    @Test
    public void checkingOfErrorMessageIfSaveEmptyAddress() {
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        driver.findElement(By.cssSelector(PROFILE_LINK_CSS)).click();
        driver.findElement(By.cssSelector(EDITING_BUTTON_CSS)).click();
        WebElement address = driver.findElement(By.id(ADDRESS_FIELD_ID));
        address.clear();
        driver.findElement(By.cssSelector(SAVING_BUTTON_CSS)).click();
        WebElement errorMessageYouMustFillInThRequiredField = driver.findElement(By.cssSelector(ERROR_MESSAGE_YOU_MUST_FILL_IN_THE_REQUIRED_FIELDS_CSS));
        assertEquals("You must fill in the required field", errorMessageYouMustFillInThRequiredField.getText());
    }

    //Change address. Test to API error
    @Test
    public void changingAddressTestForApiError() throws InterruptedException {
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        driver.findElement(By.cssSelector(PROFILE_LINK_CSS)).click();
        driver.findElement(By.cssSelector(EDITING_BUTTON_CSS)).click();
        WebElement address = driver.findElement(By.id(ADDRESS_FIELD_ID));
        address.clear();
        address.sendKeys(configAccounts.NEW_ADDRESS);
        Thread.sleep(5000);
        address.submit();
        driver.findElement(By.cssSelector(SAVING_BUTTON_CSS)).click();
        WebElement popupWithApiError = driver.findElement(By.cssSelector(POPUP_WITH_API_ERROR_CSS));
        if (popupWithApiError.getAttribute("innerHTML") != "") {
            System.out.println("API error occurred");
        }
        assertEquals("", popupWithApiError.getText());
    }

    //Checking saving new address after fixing API bug
    //
    //

    //Checking changing phone cod after fixing API bug
    //
    //

    //Checking appearing API errors after changing Phone Code
    @Test
    public void changingPhoneCodeApiErrorTest() throws InterruptedException {
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        driver.findElement(By.cssSelector(PROFILE_LINK_CSS)).click();
        driver.findElement(By.cssSelector(EDITING_BUTTON_CSS)).click();
        driver.findElement(By.cssSelector(PHONE_CODE_DROPDOWN_CSS)).click();
        String selectedCode = driver.findElement(By.cssSelector(SELECTED_PHONE_CODE_CSS)).getText();
        List<WebElement> arrayOfPhoneCodes = driver.findElements(By.cssSelector(LIST_OF_PHONE_CODES_CSS));
        String result = "";
        for (WebElement element: arrayOfPhoneCodes){
            String value = element.getAttribute("data-raw-value");
            if (!value.equals(selectedCode)){
                result = value;
            } else {
                continue;
            }
        }

    }

    //Checking API error after saving new phone number
    @Test
    public void changingPhoneNumberApiErrorTest() {
        driver.findElement(By.cssSelector(PRIVATE_AREA_LINK_CSS)).click();
        driver.findElement(By.cssSelector(PROFILE_LINK_CSS)).click();
        driver.findElement(By.cssSelector(EDITING_BUTTON_CSS)).click();
        driver.findElement(By.id(PHONE_NUMBER_FIELD_ID)).clear();
        driver.findElement(By.id(PHONE_NUMBER_FIELD_ID)).sendKeys(NEW_PHONE_NUMBER);
        driver.findElement(By.cssSelector(SAVING_BUTTON_CSS)).click();
        WebElement popupWithApiError = driver.findElement(By.cssSelector(POPUP_WITH_API_ERROR_CSS));
        if (popupWithApiError.getAttribute("innerHTML") != "") {
            System.out.println("API error occurred");
        }
        assertEquals("", popupWithApiError.getText());
    }

    @After
    public void closeBrowser() {
        driver.close();
        driver.quit();
    }
}

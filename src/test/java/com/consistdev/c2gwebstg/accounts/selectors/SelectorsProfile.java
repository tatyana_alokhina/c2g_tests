package com.consistdev.c2gwebstg.accounts.selectors;


public class SelectorsProfile {

    public static String NEW_PHONE_NUMBER = "0000000000";

    public static String LOGIN_FIELD_ID = "loginform-identity_id";
    public static String PASSWORD_FIELD_ID = "loginform-password";
    public static String CHOOSE_MEMBER_ID = "member-private-4019002";
    public static String BROWSE_PROFILE_PHOTO_ID = "edit-photo-account";
    public static String ADDRESS_FIELD_ID = "profile-address";
    public static String PHONE_NUMBER_FIELD_ID = "profile-phone";


    public static String ENGLISH_CN = "eng";
    public static String BUTTON_LOGIN_CN = "big_btn__btn";

    public static String PRIVATE_AREA_LINK_CSS = ".uk-navbar-nav.uk-hidden-small.uk-grid>li>a[href=\"/account/order/index\"]";
    public static String PROFILE_LINK_CSS = ".uk-nav.uk-nav-parent-icon >li>a[href=\"/account/profile/index\"]";
    public static String EDITING_BUTTON_CSS = ".orange_btn>a";
    public static String NAME_FIELD_CSS = ".uk-form>dl>dd";
    public static String SAVING_BUTTON_CSS = ".account_profile_main__info .orange_btn";
    public static String POPUP_WITH_API_ERROR_CSS = ".message-box .text p";
    public static String ERROR_MESSAGE_TO_BIG_PROFILE_PHOTO_CSS = "#error-container .orange-text";
    public static String ERROR_MESSAGE_THE_ADDRESS_DOES_NOT_EXIST_CSS = ".uk-margin-top-remove.orange-text";
    public static String ERROR_MESSAGE_YOU_MUST_FILL_IN_THE_REQUIRED_FIELDS_CSS = "#error-container .orange-text";
    public static String PHONE_CODE_DROPDOWN_CSS = ".pick-date > div.fancy-select > div.trigger";
    public static String SELECTED_PHONE_CODE_CSS = ".options.jspScrollable .jspPane >li.selected";
    public static String LIST_OF_PHONE_CODES_CSS = ".options.jspScrollable .jspPane >li";


    public static String ACCOUNT_NUMBER_FIELD_XPATH = ".//*[@id='profile-form']/div[2]/div[1]/dl/dd";
    public static String EMAIL_FIELD_XPATH = ".//*[@id='profile-form']/div[2]/div[3]/dl/dd";
    public static String DATE_OF_BIRTH_FIELD_XPATH = ".//*[@id='profile-form']/div[2]/div[5]/dl/dd";
}

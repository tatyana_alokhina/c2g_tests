package com.consistdev.c2gwebstg.accounts.selectors;

public class SelectorsSecurity {
    public static String SELECT_HEBREW_CN = "heb";

    public static String OLD_PASSWORD = "1qazxsw2";
    public static String NEW_PASSWORD = "1qaz2wsx";

    public static String SECURITY_OLD_PASSWORD_FIELD_ID = "securityform-old_password";
    public static String SECURITY_NEW_PASSWORD_FIELD_ID = "securityform-password";
    public static String SECURITY_CONFIRM_NEW_PASSWORD_FIELD_ID = "securityform-password_confirm";

    public static String SECURITY_LINK_XPATH = "//li[3]/a";
    public static String SECURITY_SAVE_CHANGES_BUTTON_XPATH = ".//*[@id='pass-form']/div/button";
    public static String LOG_OUT_XPATH = ".//*[@id='security-index']/div[1]//ul/li[8]/a";

    public static String USERNAME_FIELD_IN_SECURITY_PAGE_CSS = "#user-info-container>dl>dd";
}
